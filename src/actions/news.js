
export const NEWS_INIT = () => {
  return {type: "NEWS_INIT"}
};

export const NEWS_GET = () => {
  return {type: "NEWS_GET"}
};

export const NEWS_GET_POST = (id) => {
  return {type: "NEWS_GET_POST", payload: id}
};

export const NEWS_GET_POST_BULK = () => {
  return {type: "NEWS_GET_POST_BULK"}
};

export const NEWS_RECEIVED = (payload) => {
  return {type: "NEWS_RECEIVED", payload}
};

export const NEWS_RECEIVED_POST = (payload) => {
  return {type: "NEWS_RECEIVED_POST", payload}
};

