import React, { Component } from 'react';
import { connect } from 'react-redux';

class PostsTable extends Component {

  render() {
    const posts = this.props.posts;
    return (
      <>
      { posts && posts.length > 0 &&
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody>
            {posts.map(post => { 
              return (
              <tr key={post.id}>
                <td>{post.id}</td>
                <td>{post.title}</td>
                <td>{post.body}</td>
              </tr>
            )})}
          </tbody>
        </table>
      }
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    posts: state.news.posts
  }
}

export default connect(mapStateToProps)(PostsTable);
