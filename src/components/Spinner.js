import React, { Component } from 'react';
import { connect } from 'react-redux';

import '../scss/spinner.scss';
import logo from '../logo.svg';

class Spinner extends Component {

  render() {
    return (
      <>
        { this.props.loading &&
          <div className="spinner">
              <img src={logo} className="spinnerLogo" alt="spinner logo" />
          </div>
        }
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.news.loading
  }
}

export default connect(mapStateToProps)(Spinner);
