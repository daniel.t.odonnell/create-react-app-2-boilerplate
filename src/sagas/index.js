import { put, takeLatest, takeEvery, all } from 'redux-saga/effects';
import axios from 'axios';

import * as NewsActions from '../actions/news';

async function stall(stallTime = 3000) {
  await new Promise(resolve => setTimeout(resolve, stallTime));
}

function* fetchNews() {
  //yield stall();
  try {
    const response = yield axios.get('https://jsonplaceholder.typicode.com/posts');
    yield put(NewsActions.NEWS_RECEIVED(response.data));
  } catch (error) {
    console.error(error);
  }
}

function* fetchNewsPost(action) {
  //yield stall();
  try {
    const response = yield axios.get('https://jsonplaceholder.typicode.com/posts/' + action.payload);
    yield put(NewsActions.NEWS_RECEIVED_POST(response.data));
  } catch (error) {
    console.error(error);
  }
}

function* fetchNewsPostBulk(action) {
  //yield stall();
  try {
    for (let i = 1; i <= 20; i++) {
      const response = yield axios.get('https://jsonplaceholder.typicode.com/posts/' + i);
      yield put(NewsActions.NEWS_RECEIVED_POST(response.data));
    }
  } catch (error) {
    console.error(error);
  }
}

function* newsWatcher() {
  yield takeLatest('NEWS_GET', fetchNews);
  yield takeEvery('NEWS_GET_POST', fetchNewsPost);
  yield takeLatest('NEWS_GET_POST_BULK', fetchNewsPostBulk);
}

export default function* rootSaga() {
  yield all([
    newsWatcher(),
  ]);
}