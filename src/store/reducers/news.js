import produce from "immer";

const initState = { posts: []}
const news = (state = initState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case 'NEWS_INIT':
        return {...initState};
      case 'NEWS_GET':
        draft.posts = [];
        draft.loading = true;
        break;
      case 'NEWS_GET_POST':
        draft.posts = [];
        draft.loading = true;
        break;
      case 'NEWS_GET_POST_BULK':
        draft.loading = true;
        break;
      case 'NEWS_RECEIVED':
        draft.posts = action.payload;
        draft.loading = false;
        break;
      case 'NEWS_RECEIVED_POST':
        draft.posts.push(action.payload);
        draft.loading = false;
        break;
      // no default
    }
  })

export default news;