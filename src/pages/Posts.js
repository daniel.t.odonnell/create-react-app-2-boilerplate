import React, { Component } from 'react';
import { connect } from 'react-redux';

import PostsTable from '../components/PostsTable';

import * as NewsActions from '../actions/news';

class Posts extends Component {

  constructor(props) {
    super(props);

    this.doClick = this.doClick.bind(this);
    this.doClickPost = this.doClickPost.bind(this);
    this.doClickPostBulk = this.doClickPostBulk.bind(this);
  }

  doClick(evt) {
    this.props.NEWS_GET();
  }
  doClickPost(evt) {
    for (let i = 1; i <= 20; i++) {
      this.props.NEWS_GET_POST(i);
    }
  }
  doClickPostBulk(evt) {
      this.props.NEWS_INIT();
      this.props.NEWS_GET_POST_BULK();
  } 
  render() {
    return (
      <>
        <button onClick={this.doClick}>Fetch 100 Posts</button>
        <button onClick={this.doClickPost}>Fetch a Post 20 times from Client</button>
        <button onClick={this.doClickPostBulk}>Fetch a Post 20 times from Saga</button>
        <PostsTable />
      </>
    );
  }
}

export default connect(null, {...NewsActions})(Posts);
