import React, { Component } from 'react';

import Spinner from './components/Spinner';

import Posts from './pages/Posts';

class App extends Component {

 
  render() {
    return (
      <div className="App">
        <Spinner />
        <Posts />
      </div>
    );
  }
}

export default App;
